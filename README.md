# PyBin

![screenshot](https://i.postimg.cc/wBP7XX8D/screenshot.png)
![screenshot](https://i.postimg.cc/fRYKCrjx/screenshot4.png)

## Quick Guide

1.  **Install packages**
         
        pip install -r requirements.txt

 

2. **Create database** 

        python from application import db
    
        python db.create_all()


3. **Run Server**

       python application.py